__author__ = 'alexey'

from flask_wtf import RecaptchaField

from flask.ext.wtf import Form

from wtforms import TextField
from wtforms import PasswordField
from wtforms import BooleanField
from wtforms import TextAreaField
from wtforms import SubmitField

from wtforms.validators import Required

class LoginForm(Form):
    nickname = TextField('nickname', validators=[Required()])
    password = PasswordField('password', validators=[Required()])
    remember_me = BooleanField('remember_me', default=False)
    # recaptcha = RecaptchaField()


class SigninForm(Form):
    nickname = TextField('nickname', validators=[Required()])
    password = PasswordField('password', validators=[Required()])
    telephone = TextField('telephone', validators=[Required()])
    email = TextField('email', validators=[Required()])
    firstName = TextField('firstName', validators=[Required()])
    secondName = TextField('secondName', validators=[Required()])
    e_address = TextField('e_address', validators=[Required()])

class CommentForm(Form):
    header = TextField('header', validators=[Required()])
    comment = TextAreaField('comment', validators=[Required()])

class PropertyForm(Form):
    price = TextField('price', validators=[Required()])
    count = TextField('count', validators=[Required()])

class SearchProductForm(Form):
    model = TextField('model')

    price_from = TextField('price_from')
    price_to = TextField('price_to')

    count_from = TextField('count_from')
    count_to = TextField('count_to')