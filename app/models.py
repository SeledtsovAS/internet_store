__author__ = 'alexey'

from app import db
from datetime import datetime

class TypeUser(object):
    """
        description type of user in application:
            admin -- own all rights on changed system info and add new product
            user -- own limited rights.
    """
    ROLE_USER, ROLE_ADMIN = 0x000, 0x001

    @staticmethod
    @property
    def __getattr__(self, item):
        return self.item

class FirstName(db.Model):
    """
        table first names all users
            column: id and first name
    """
    __tablename__ = 'first_names'

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(40), index=True, unique=True)

    def __repr__(self):
        return "<id: {}, first_name: {}.>".format(self.id, self.first_name)

    @staticmethod
    def repeateAddFirstName(first_name):
        firstname = db.session.query(FirstName).filter(FirstName.first_name == first_name).first()
        if firstname is None:
            firstname = FirstName(first_name=first_name)
            db.session.add(firstname)
            db.session.commit()
        return firstname.id

class SecondName(db.Model):
    """
        table second names all users
            column: id and second_name
    """
    __tablename__ = 'second_names'

    id = db.Column(db.Integer, primary_key=True)
    second_name = db.Column(db.String(40), index=True, unique=True)

    def __repr__(self):
        return "<id: {}, second_name: {}.>".format(self.id, self.second_name)

    @staticmethod
    def repeateAddSecondName(second_name):
        secondname = db.session.query(SecondName).filter(SecondName.second_name == second_name).first()
        if secondname is None:
            secondname = SecondName(second_name=second_name)
            db.session.add(secondname)
            db.session.commit()
        return secondname.id

class Password(db.Model):
    """
        table password save encrypted password of users
            columns: id, data(encrypted password)
    """
    __tablename__ = 'password'

    id = db.Column(db.Integer, primary_key=True)
    data = db.Column(db.String(256))

    def __repr__(self):
        return "<id: {}, password: {}>".format(self.id, self.data)

    @staticmethod
    def repeateAddPassword(password):
        passwordobj = db.session.query(Password).filter(Password.data == password).first()
        if passwordobj is None:
            passwordobj = Password(data=password)
            db.session.add(passwordobj)
            db.session.commit()
        return passwordobj.id

class BankCard(db.Model):
    """
        table bank cards save all data of users cards:
            columns: id, el_address(email), balance
    """
    __tablename__ = 'bank_cards'

    id = db.Column(db.Integer, primary_key=True)
    el_address = db.Column(db.String(12), unique=True, index=True)
    balance = db.Column(db.Float)

    def __repr__(self):
        return "<id: {}, email: {}, balance: {}>".format(self.id, self.el_address, self.balance)

    @staticmethod
    def repeateAddBankCard(e_address):
        bankCard = db.session.query(BankCard).filter(BankCard.el_address == e_address).first()
        if bankCard is None:
            bankCard = BankCard(el_address=e_address, balance=120000.0)
            db.session.add(bankCard)
            db.session.commit()
        return bankCard.id

class User(db.Model):
    """
        table user describes user of web-site
            column:
                id - primary key
                nickname - identify of user in website
                email
                telephone
                id_first_names, id_second_names and etc is foreign key
    """
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String(120), unique=True, index=True, default=None)
    email = db.Column(db.String(30), unique=True, index=True)
    telephone = db.Column(db.String(12), unique=True, index=True)
    role = db.Column(db.SmallInteger, default=TypeUser.ROLE_USER)

    id_first_names = db.Column(db.Integer, db.ForeignKey('first_names.id'))
    id_second_names = db.Column(db.Integer, db.ForeignKey('second_names.id'))
    id_password = db.Column(db.Integer, db.ForeignKey('password.id'))
    id_bank_cards = db.Column(db.Integer, db.ForeignKey('bank_cards.id'))

    def __repr__(self):
        return "User: {0:s}".format(self.nickname)

    def __str__(self):
        return "User: {0:s}".format(self.nickname)

    @staticmethod
    def is_authenticated():
        return True

    @staticmethod
    def is_active():
        return True

    @staticmethod
    def is_anonimous():
        return False

    @staticmethod
    def getNextNickname(nickname):
        index = 1
        while User.isExistNickname(nickname + str(index)):
            index += 1
        return nickname + str(index)

    @staticmethod
    def isExistNickname(nickname):
        res = db.session.query(User).filter(User.nickname == nickname).first()
        return res

    @staticmethod
    def getIdTelephone(telephone):
        res = db.session.query(User).filter(User.telephone == telephone).first()
        return res

    @staticmethod
    def getIdEmail(email):
        res = db.session.query(User).filter(User.email == email).first()
        return res

    def get_id(self):
        return self.id

class ProductType(object):
    """
        product type constants
    """
    COMPUTER = 0x00
    MONITOR = 0x01
    PRINTER = 0x02
    KEYBOARD = 0x03
    MOUSE = 0x04
    FILTER = 0x05
    SERVER = 0x06

    @staticmethod
    @property
    def __getattr__(self, item):
        return self.item

class Image(db.Model):
    """
        table of all images in application
        columns:
            - id
            - url image on computer
    """
    __tablename__ = 'image'
    id = db.Column(db.Integer, primary_key = True)
    url = db.Column(db.String(220), default = None, unique=True)

    def __repr__(self):
        return "<id: {}, url: {}>".format(self.id, self.url)

class Product(db.Model):
    """
        table all product in application
        columns:
            id - identify of product
            name - call of product for consumer
            count - count products
            type - computer, filter, server, mouse, keyboard
            price
            id_image - pointer on associated image in table images
    """
    __tablename__ = 'product'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40), index=True)
    count = db.Column(db.Integer)
    type = db.Column(db.SmallInteger, default=ProductType.COMPUTER, index=True)
    price = db.Column(db.Float(2), default=1000.0)

    id_image = db.Column(db.Integer, db.ForeignKey('image.id'))

    def __repr__(self):
        return "<id: {}, name: {}, count: {}, type: {}, price: {}, id_image: {}>".format(
            self.id, self.name, self.count, self.type, self.price, self.id_image
        )

class Cpu(db.Model):
    """
        table cpu
        columns:
            id
            description - model, work frequency and etc
    """
    __tablename__ = 'cpu'

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(50), index=True, unique=True)

    def __repr__(self):
        return "<id: {}, description: {}>".format(self.id, self.description)


class VideoCard(db.Model):
    """
        table videoCard
        columns:
            id
            description - model, volume of memory, work frequency
    """
    __tablename__ = 'videoCard'

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(50), index=True, unique=True)

    def __repr__(self):
        return "<id: {}, description: {}>".format(self.id, self.description)

class SoundCard(db.Model):
    """
        table SoundCard
        columns:
            id
            description - model, power, work frequency, volume of memory
    """
    __tablename__ = 'soundCard'

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(50), index=True, unique=True)

    def __repr__(self):
        return "<id: {}, description: {}>".format(self.id, self.description)

class Memory(db.Model):
    """
        table memory
        columns:
            id
            description: volume of memory, work frequency, model
    """
    __tablename__ = 'memory'

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(50), index=True, unique=True)

    def __repr__(self):
        return "<id: {}, description: {}>".format(self.id, self.description)

class Hdd(db.Model):
    """
        table hdd
        columns:
            id
            description - model, work frequency, power, volume of memory
    """
    __tablename__ = 'hdd'

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(50), index=True, unique=True)

    def __repr__(self):
        return "<id: {}, description: {}>".format(self.id, self.description)

class Computer(db.Model):
    """
        table computer
        columns:
            id
            model: string: model of computer
            id_cpu: int: pointer on cpu from table cpu
            id_videoCard: int: pointer on videoCard from table videoCard
            id_memory: int: pointer on memory from table memory
            id_soundCard: int: pointer on soundCard from table soundCard
            id_product: int: pointer on product from table Product
    """
    __tablename__ = 'computer'

    id = db.Column(db.Integer, primary_key=True)
    model = db.Column(db.String(30), index=True)

    id_cpu = db.Column(db.Integer, db.ForeignKey('cpu.id'))
    id_videoCard = db.Column(db.Integer, db.ForeignKey('videoCard.id'))
    id_memory = db.Column(db.Integer, db.ForeignKey('memory.id'))
    id_soundCard = db.Column(db.Integer, db.ForeignKey('soundCard.id'))
    id_hdd = db.Column(db.Integer, db.ForeignKey('hdd.id'))

    id_product = db.Column(db.Integer, db.ForeignKey('product.id'))

    def __repr__(self):
        return "<id: {}, model: {}, cpu: {}, memory: {}, sound: {}, hdd: {}, product: {}>".format(
            self.id, self.model, self.id_cpu, self.id_memory, self.id_soundCard, self.id_hdd,
            self.id_product
        )

class Producer(db.Model):
    """
        table producer
        columns:
            id: int
            name: string: call of producer from consumer
    """
    __tablename__ = 'producer'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), unique=True, index=True)

    def __repr__(self):
        return "<id: {}, name: {}>".format(self.id, self.name)

class Color(db.Model):
    """
        table color
            id: int
            color: string: description of color
    """
    __tablename__ = 'color'

    id = db.Column(db.Integer, primary_key = True)
    color = db.Column(db.String(20), unique=True, index=True)

    def __repr__(self):
        return "<id: {}, name: {}>".format(self.id, self.color)

class Resolution(db.Model):
    """
        table resolution
            id: int: identify of resolution row
            width: int: width of the diagonal
            height: int: height on the diagonal
    """
    __tablename__ = 'resolution'

    id = db.Column(db.Integer, primary_key=True)
    width = db.Column(db.Integer, index=True)
    height = db.Column(db.Integer, index=True)

    def __repr__(self):
        return "<width: {}, height: {}>".format(self.width, self.height)

class Monitor(db.Model):
    """
        table monitor
        columns:
            id: int: identify of monitor row
            model: string(30): call monitor for consumer
            countColors: bigint: count of colors on display
            id_producer: int: pointer on monitor row
            id_color: int: pointer on color in table colors
            id_resolution: int: pointer on resolution
            id_product: int: pointer on product
    """
    __tablename__ = 'monitor'

    id = db.Column(db.Integer, primary_key=True)
    model = db.Column(db.String(30), index=True)
    countColors = db.Column(db.BigInteger)

    id_producer = db.Column(db.Integer, db.ForeignKey('producer.id'))
    id_color = db.Column(db.Integer, db.ForeignKey('color.id'))
    id_resolution = db.Column(db.Integer, db.ForeignKey('resolution.id'))
    id_product = db.Column(db.Integer, db.ForeignKey('product.id'))

    def __repr__(self):
        return "<id: {}, model: {}, countColors: {}, producer: {}, color: {}, resolution: {}, product: {}>".format(
            self.id, self.model, self.countColors, self.id_producer, self.id_color, self.id_resolution, self.id_product
        )

class Filter(db.Model):
    """
        table filter
        columns:
            id: int: identify of filter in table filter
            model: string: call of filter for consumer
            length: int: length of cabel filter
            power: double: consumered power in vat
    """
    __tablename__ = 'filter'

    id = db.Column(db.Integer, primary_key=True)
    model = db.Column(db.String(30), index=True)
    length = db.Column(db.Float(2))
    power = db.Column(db.Float(2))

    id_product = db.Column(db.Integer, db.ForeignKey('product.id'))

    def __repr__(self):
        return "<id: {}, model: {}, length: {}, power: {}, id_product: {}>".format(
            self.id, self.model, self.length, self.power, self.id_product
        )

class Keyboard(db.Model):
    """
        table of keyboard
        columns:
            id: int
            model: string(30): call of keyboard for consumers
            id_producer: int: pointer on row in table producer
            id_color: int: pointer on row id in table color
            id_product: int: pointer on row id in table product
    """
    id = db.Column(db.Integer, primary_key=True)
    model = db.Column(db.String(30))

    id_producer = db.Column(db.Integer, db.ForeignKey('producer.id'))
    id_color = db.Column(db.Integer, db.ForeignKey('color.id'))
    id_product = db.Column(db.Integer, db.ForeignKey('product.id'))

    def __repr__(self):
        return "id: {}, model: {}, producer: {}, color: {}, product: {}".format(
            self.id, self.model, self.producer, self.color, self.product
        )

class Interface(db.Model):
    """
        table interface
        columns:
            id: int
            interface: string: usb, usb2.0, wlan, sata, ata, com-port
    """
    __tablename__ = 'interface'

    id = db.Column(db.Integer, primary_key=True)
    interface = db.Column(db.String(20), index=True, unique=True)

    def __repr__(self):
        return "<id: {}, interface: {}>".format(self.id, self.interface)

class Mouse(db.Model):
    """
        table mouse
        columns:
            id: int
            model: string: call of mouse for consumer
            id_producer: int: pointer on id row in table producer
            id_interface: int: pointer on id row in table interface
            id_color: int: pointer on id row in table color
            id_product: int: pointer on id row in table product
    """
    __tablename__ = 'mouse'

    id = db.Column(db.Integer, primary_key=True)
    model = db.Column(db.String(30), index=True)

    id_producer = db.Column(db.Integer, db.ForeignKey('producer.id'))
    id_interface = db.Column(db.Integer, db.ForeignKey('interface.id'))
    id_color = db.Column(db.Integer, db.ForeignKey('color.id'))
    id_product = db.Column(db.Integer, db.ForeignKey('product.id'))

class Server(db.Model):
    """
        table server
        columns:
            id: int
            model: string: call server for consumer
            id_producer: int: pointer on row id in table producer
            id_hdd: int: pointer on id row in table hdd
            id_cpu: int: pointer on id row in table cpu
            id_memory: int: pointer on id row in table memory
            id_product: int: pointer on id row in table product
    """
    __tablename__ = 'server'

    id = db.Column(db.Integer, primary_key=True)
    model = db.Column(db.String(30), index=True)

    id_producer = db.Column(db.Integer, db.ForeignKey('producer.id'))
    id_hdd = db.Column(db.Integer, db.ForeignKey('hdd.id'))
    id_cpu = db.Column(db.Integer, db.ForeignKey('cpu.id'))
    id_memory = db.Column(db.Integer, db.ForeignKey('memory.id'))
    id_product = db.Column(db.Integer, db.ForeignKey('product.id'))

class Case(db.Model):
    """
        table case (transaction table between table user and product, majority on majority)
        columns:
            id: int
            id_user: pointer on id row in table user
            id_product: pointer on id row in table product
    """
    __tablename__ = 'case'

    id = db.Column(db.Integer, primary_key=True)

    id_user = db.Column(db.Integer, db.ForeignKey('user.id'))
    id_product = db.Column(db.Integer, db.ForeignKey('product.id'))

    def __repr__(self):
        return '<id: {}, id_user: {}, id_product: {}>'.format(self.id, self.id_user, self.id_product)

class Post(db.Model):
    """
        table post
        columns:
            id: int
            body: string: information about consist in post
            date: datetime: information about create post

            id_user: int: pointer on id row in table user
            id_product: int: pointer on id row in table product
    """
    __tablename__ = 'post'

    id = db.Column(db.Integer, primary_key=True)
    header = db.Column(db.String(30))
    body = db.Column(db.String(200))
    date = db.Column(db.DateTime, default=datetime.utcnow())

    id_user = db.Column(db.Integer, db.ForeignKey('user.id'))
    id_product = db.Column(db.Integer, db.ForeignKey('product.id'))

    def __repr__(self):
        return "<id: {}, body: {}, date: {}, id_user: {}, id_product: {}>".format(
            self.id, self.body, self.date, self.id_user, self.id_product
        )

class BuyProduct(db.Model):
    __name__ = 'buy_product'
    id = db.Column(db.Integer, primary_key=True)
    time = db.Column(db.DateTime, default=datetime.utcnow())
    id_user = db.Column(db.Integer, db.ForeignKey('user.id'))
    id_product = db.Column(db.Integer, db.ForeignKey('product.id'))