__author__ = 'alexey'

from flask import Flask
from flask.ext.login import LoginManager
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__)

# configuration app from config.py
app.config.from_object('config')

# создаем менеджер авторизации
# и инициализируем его приложением
# для совместной работы
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

db = SQLAlchemy(app)

from app import views, models