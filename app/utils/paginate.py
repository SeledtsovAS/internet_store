__author__ = 'alexey'

from math import ceil

class Pagination(object):
    def __init__(self, page, total_count, per_page):
        self.page = page
        self.total_count = total_count
        self.per_page = per_page

    @property
    def pages(self):
        return ceil(self.total_count / self.per_page)

    @property
    def has_next(self):
        return self.page < self.pages

    @property
    def has_prev(self):
        return self.page > 1

    def iter_pages(self):
        for num in range(1, self.pages + 1):
            yield num
        else:
            yield None