__author__ = 'alexey'

from os import listdir
from os.path import isdir
from os.path import join

class FileIndexer(object):
    """
        FileIndexer - индексатор файлов в каталоге,
        предназначенный для индексации файлов изображений
    """
    @staticmethod
    def getImagesFrom(path):
        res = []
        for file in listdir(path):
            if not isdir(file):
                res.append(join('img/', file))
        return res