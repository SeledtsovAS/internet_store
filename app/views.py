__author__ = 'alexey'

from random import choice
from datetime import datetime

from app import app
from app import login_manager
from app import db

from flask import session
from flask import abort
from flask import request
from flask import render_template
from flask import g
from flask import redirect
from flask import url_for

from flask.ext.login import login_user
from flask.ext.login import logout_user
from flask.ext.login import current_user
from flask.ext.login import login_required

from .forms import LoginForm
from .forms import SigninForm
from .forms import CommentForm
from .forms import PropertyForm
from .forms import SearchProductForm

from .models import User
from .models import Password
from .models import FirstName
from .models import SecondName
from .models import BankCard
from .models import TypeUser
from .models import Image
from .models import Computer
from .models import Product
from .models import Memory
from .models import Hdd
from .models import Cpu
from .models import VideoCard
from .models import SoundCard
from .models import Post
from .models import Case
from .models import Server
from .models import Monitor
from .models import Filter
from .models import Mouse
from .models import Keyboard
from .models import Producer
from .models import Color
from .models import Interface
from .models import Resolution
from .models import BuyProduct

from app.utils.paginate import Pagination

def get_type(catalog):
    return {
        'computer': 0,
        'server': 6,
        'mouse': 4,
        'monitor': 1,
        'filter': 5,
        'keyboard': 3
    }.get(catalog, None)

def get_type_for_number(num):
    return {
        0: 'computer',
        1: 'monitor',
        3: 'keyboard',
        4: 'mouse',
        5: 'filter',
        6: 'server'
    }.get(num, None)

def get_product_for_page(page, per_page, result):
    start = (page - 1) * per_page
    return result[start: start + per_page]

per_page = 2

@app.route('/cancel_buy_product/<int:id>/', methods=['GET', 'POST'])
@login_required
def cancel_buy_product(id):
    query = db.session.query(Case).filter(Case.id_product == id, Case.id_user == current_user.id).first()

    product = db.session.query(Product).filter(Product.id == id).first()
    product.count += 1

    if query:
        db.session.delete(query)
        db.session.commit()

    return redirect(session.get('prev_url', '/'))

@app.route('/change_property_for_product/<int:id>/', methods=['GET', 'POST'])
@login_required
def change_property_for_product(id):
    query = db.session.query(Product, Image).filter(Product.id == id, Image.id == Product.id_image).first()
    form = PropertyForm()

    if form.validate_on_submit():
        query[0].price = float(form.price.data)
        query[0].count = int(form.count.data)
        db.session.commit()

        form.count.data = ""
        form.price.data = ""

        return render_template('changerproperty.html', form=form, product=query, change=1, success=1)

    return render_template('changerproperty.html', form=form, product=query, change=1)

@app.route('/change/<int:on_page>')
def change_per_page(on_page):
    global per_page
    per_page = on_page
    return redirect(session.get('prev_url', '/'))

def get_url_for_full_description(kind, products):
    return {product[0].id: '/catalog/' + kind + '/description/' + str(product[0].id) for product in products}

def filter_result(products, form):
    if form.validate_on_submit():
        if request.form.get('count_from'):
            products = products.filter(Product.count >= int(request.form.get('count_from')))
        if request.form.get('count_to'):
            products = products.filter(Product.count <= int(request.form.get('count_to')))
        if request.form.get('price_from'):
            products = products.filter(Product.price >= float(request.form.get('price_from')))
        if request.form.get('price_to'):
            products = products.filter(Product.price <= float(request.form.get('price_to')))
        if request.form.get('model'):
            products = products.filter(Product.name.contains(request.form.get('model')))
    return products

result_query = None

@app.route('/catalog/<kind>/<int:page>', methods=['GET', 'POST'])
@app.route('/catalog/<kind>/', defaults={'page': 1}, methods=['GET', 'POST'])
@login_required
def catalog(kind, page):
    ptype = get_type(kind)
    global result_query
    if page == 0:
        result_query = None
        return redirect(url_for('catalog', kind=kind))

    if result_query is None:
        products = db.session.query(Product, Image).filter(Product.type == ptype, Image.id == Product.id_image)
        result_query = products
    else:
        products = result_query

    form = SearchProductForm()
    products = filter_result(products, form)
    result_query = products

    total_count = len(list(products))
    products = get_product_for_page(page, per_page, list(products))
    if not products and page != 1:
       return redirect(url_for('catalog', kind=kind))
    urls = get_url_for_full_description(kind, products)
    pagination = Pagination(page, total_count, per_page)

    session['prev_url'] = request.path
    return render_template(
        'catalog.html', page=page, products=products, pagination=pagination, urls=urls, form=form,
        consist_case=get_summa_in_case(current_user.id)
    )

@app.route('/admin/user_delete/<int:id>', methods=['GET', 'POST'])
@login_required
def admin_user_delete(id):
    print('delete user')
    return redirect(session.get('prev_url', '/'))

@app.route('/admin/user_add_balance/<int:id>', methods=['GET', 'POST'])
@login_required
def user_add_balance(id):
    balance = db.session.query(User, BankCard).filter(
        User.id_bank_cards == BankCard.id, User.id == id
    ).first()

    balance[1].balance += 1000
    try:
        db.session.commit()
    except:
        db.session.rollback()

    return redirect(session.get('prev_url', '/'))

@app.route('/admin/panel/', methods=['GET', 'POST'])
@login_required
def admin_panel():
    transactions = db.session.query(BuyProduct, User, Product, FirstName, SecondName).filter(
        BuyProduct.id_user == User.id, BuyProduct.id_product == Product.id,
        User.id_first_names == FirstName.id, User.id_second_names == SecondName.id
    )
    users = db.session.query(User, FirstName, SecondName, BankCard).filter(
        User.id_first_names == FirstName.id, User.id_second_names == SecondName.id, BankCard.id == User.id_bank_cards
    )
    session['prev_url'] = request.path
    return render_template(
        'buyproduct.html', transactions=transactions, get_type=get_type_for_number, users=users
    )

@app.route('/buy_product/<int:id>/', methods=['GET', 'POST'])
@login_required
def buy_product(id):
    case = db.session.query(Case, Product).filter(
        Case.id_user == current_user.id, Case.id_product == id, Product.id == id
    ).first()

    user = db.session.query(User, BankCard, FirstName, SecondName).filter(
        User.id == current_user.id, User.id_bank_cards == BankCard.id,
        User.id_first_names == FirstName.id, User.id_second_names == SecondName.id
    ).first()

    result = None
    if case is None:
        case = db.session.query(Product).filter(Product.id == id).first()
        if case.count > 0 and case.price <= user[1].balance:
            case.count -= 1
            user[1].balance -= case.price
            result = BuyProduct(id_user=current_user.id, id_product=case.id)
    else:
        if case[1].count > 0 and case[1].price <= user[1].balance:
            user[1].balance -= case[1].price
            result = BuyProduct(id_user=current_user.id, id_product=case[1].id)
            db.session.delete(case[0])
    if result is not None:
        db.session.add(result)
    db.session.commit()

    result = db.session.query(Product).filter(Product.id == id).first()
    type = get_type_for_number(result.type)
    return render_template(
        'buypage.html', user=user, product=result, time=datetime.utcnow(), type=type,
        url=session.get('prev_url', '/')
    )

def get_summa_in_case(id_user):
    cases = db.session.query(Case, Product).filter(Case.id_user == id_user, Product.id == Case.id_product).all()
    summa = 0
    for case in cases:
        summa += case[1].price
    return summa, len(cases)

@app.route('/add_into_case/<int:id>/', methods=['GET', 'POST'])
@login_required
def add_into_case(id):
    product = db.session.query(Product).filter(Product.id == id).first()
    user = db.session.query(User, BankCard).filter(User.id_bank_cards == BankCard.id, User.id == current_user.id).first()
    case = Case(id_user=current_user.id, id_product=id)
    print(get_summa_in_case(current_user.id))
    if product.count <= 0 or user[1].balance < product.price + get_summa_in_case(current_user.id)[0]:
        return redirect(session.get('prev_url', '/'))
    product.count -= 1

    try:
        db.session.add(case)
        db.session.commit()
    except:
        db.session.rollback()

    return redirect(session.get('prev_url', '/'))

def get_comments(id_product):
    return db.session.query(Post, User, Product).filter(
        Post.id_product == Product.id, Post.id_user == User.id, Post.id_product == id_product
    ).all()

def add_comment_into_database(form, id):
    if form.validate_on_submit():
        post = Post(body=form.comment.data, header=form.header.data, id_user=current_user.id, id_product=id)
        db.session.add(post)
        db.session.commit()

        form.comment.data = ""
        form.header.data = ""

@app.route('/current_client_case/<int:id>/', methods=['GET', 'POST'])
@login_required
def current_client_case(id):
    query = db.session.query(Product, Image, Case, User).filter(
        Case.id_product == Product.id, Case.id_user == User.id, User.id == id, Image.id == Product.id_image
    ).all()

    urls = {
        elem[0].id: '/catalog/' + get_type_for_number(elem[0].type) + '/description/' + str(elem[0].id) + '/'
        for elem in query
    }

    user = db.session.query(User, FirstName, SecondName, BankCard).filter(
        User.id_first_names == FirstName.id, User.id_second_names == SecondName.id,
        User.id_bank_cards == BankCard.id, User.id == id
    ).first()

    session['prev_url'] = request.path
    return render_template(
        'clientcase.html', products=query, urls=urls, user=user, case=1,
        consist_case=get_summa_in_case(current_user.id)
    )

@app.route('/catalog/computer/description/<int:page>/', methods=['GET', 'POST'])
@app.route('/catalog/computer/description/', defaults={'page': 1}, methods=['GET', 'POST'])
@login_required
def computer_description(page):
    query = db.session.query(Image, Product, Computer, Cpu, Memory, Hdd, VideoCard, SoundCard).filter(
        Image.id == Product.id_image, Product.id == Computer.id_product, Computer.id_cpu == Cpu.id,
        Computer.id_memory == Memory.id, Hdd.id == Computer.id_hdd, Computer.id_videoCard == VideoCard.id,
        Computer.id_soundCard == SoundCard.id, Product.id == page
    ).first()

    form = CommentForm()
    add_comment_into_database(form, page)

    comments = get_comments(page)
    session['prev_url'] = request.path
    return render_template(
        'fulldescription.html', kind='computer.html', product=query, comments=comments, form=form
    )

@app.route('/catalog/server/description/<int:page>/', methods=['GET', 'POST'])
@app.route('/catalog/server/description/', defaults={'page': 1}, methods=['GET', 'POST'])
@login_required
def server_description(page):
    query = db.session.query(Image, Product, Server, Producer, Cpu, Memory, Hdd).filter(
        Image.id == Product.id_image, Server.id_product == Product.id, Server.id_producer == Producer.id,
        Server.id_cpu == Cpu.id, Server.id_memory == Memory.id, Server.id_hdd == Hdd.id, Product.id == page
    ).first()
    print(query)
    form = CommentForm()
    add_comment_into_database(form, page)

    comments = get_comments(page)
    session['prev_url'] = request.path
    return render_template(
        'fulldescription.html', kind='server.html', product=query, comments=comments, form=form
    )

@app.route('/catalog/filter/description/<int:page>/', methods=['GET', 'POST'])
@app.route('/catalog/filter/description/', defaults={'page': 1}, methods=['GET', 'POST'])
@login_required
def filter_description(page):
    query = db.session.query(Image, Product, Filter).filter(
        Image.id == Product.id_image, Filter.id_product == Product.id, Product.id == page
    ).first()

    form = CommentForm()
    add_comment_into_database(form, page)

    comments = get_comments(page)
    session['prev_url'] = request.path
    return render_template(
        'fulldescription.html', kind='filter.html', product=query, comments=comments, form=form
    )

@app.route('/catalog/mouse/description/<int:page>/', methods=['GET', 'POST'])
@app.route('/catalog/mouse/description/', defaults={'page': 1}, methods=['GET', 'POST'])
@login_required
def mouse_description(page):
    query = db.session.query(Image, Product, Mouse, Color, Producer, Interface).filter(
        Image.id == Product.id_image, Product.id == Mouse.id_product, Mouse.id_color == Color.id,
        Mouse.id_producer == Producer.id, Mouse.id_interface == Interface.id, Product.id == page
    ).first()

    form = CommentForm()
    add_comment_into_database(form, page)

    comments = get_comments(page)
    session['prev_url'] = request.path
    return render_template(
        'fulldescription.html', kind='mouse.html', product=query, comments=comments, form=form
    )

@app.route('/catalog/keyboard/description/<int:page>/', methods=['GET', 'POST'])
@app.route('/catalog/keyboard/description/', defaults={'page': 1}, methods=['GET', 'POST'])
@login_required
def keyboard_description(page):
    query = db.session.query(Image, Product, Keyboard, Producer, Color).filter(
        Image.id == Product.id_image, Product.id == Keyboard.id_product, Keyboard.id_producer == Producer.id,
        Keyboard.id_color == Color.id, Product.id == page
    ).first()

    form = CommentForm()
    add_comment_into_database(form, page)

    comments = get_comments(page)
    session['prev_url'] = request.path
    return render_template(
        'fulldescription.html', kind='keyboard.html', product=query, comments=comments, form=form
    )

@app.route('/catalog/monitor/description/<int:page>/', methods=['GET', 'POST'])
@app.route('/catalog/monitor/description/', defaults={'page': 1}, methods=['GET', 'POST'])
@login_required
def monitor_description(page):
    query = db.session.query(Image, Product, Monitor, Producer, Color, Resolution).filter(
        Image.id == Product.id_image, Monitor.id_product == Product.id, Monitor.id_producer == Producer.id,
        Monitor.id_color == Color.id, Monitor.id_resolution == Resolution.id, Product.id == page
    ).first()

    form = CommentForm()
    add_comment_into_database(form, page)

    comments = get_comments(page)
    session['prev_url'] = request.path
    return render_template(
        'fulldescription.html', kind='monitor.html', product=query, comments=comments, form=form
    )

@app.route('/', methods=['GET', 'POST'])
@app.route('/index/', methods=['GET', 'POST'])
@login_required
def index():
    return render_template('index.html', title="Main page")

@login_manager.user_loader
def user_loader(userid):
    return User.query.get(userid)

@app.route('/logout/')
def logout():
    logout_user()
    return redirect(url_for('login'))

@app.route('/login/', methods=['GET', 'POST'])
def login():
    if hasattr(g, 'user') and g.user.is_authenticated():
        return redirect(url_for('index'))

    error = ""
    form = LoginForm()
    if form.validate_on_submit():
        nickname = request.form['nickname']
        password = request.form['password']

        user = db.session.query(User, Password).filter(User.nickname == nickname, User.id_password == Password.id,
                                                       Password.data == password).first()
        if user is not None:
            login_user(user[0], remember=request.form.get('remember_me'))
            return redirect(request.args.get('next') or url_for('index'))
        else:
            error = "Incorrect login or password"
    return render_template("login.html", title="Login", form=form, error=error)


def url_for_other_page(page):
    args = request.view_args.copy()
    args['page'] = page
    return url_for(request.endpoint, **args)

app.jinja_env.globals['url_for_other_page'] = url_for_other_page

@app.route('/signin/', methods=['GET', 'POST'])
def signin():
    form = SigninForm()

    err_email, err_telephone = None, None
    nickname = None
    if form.validate_on_submit():
        nickname = request.form['nickname']
        telephone = request.form['telephone']
        email = request.form['email']
        role = TypeUser.ROLE_USER

        if not User.isExistNickname(nickname):
            firstname = FirstName.repeateAddFirstName(request.form['firstName'])
            secondname = SecondName.repeateAddSecondName(request.form['secondName'])
            bankCard = BankCard.repeateAddBankCard(request.form['e_address'])
            password = Password.repeateAddPassword(request.form['password'])

            err_email = User.getIdEmail(email)
            err_telephone = User.getIdTelephone(telephone)
            if err_email is None and err_telephone is None:
                user = User(nickname=nickname, telephone=telephone, email=email, id_first_names=firstname,
                            id_second_names=secondname, id_password=password, id_bank_cards=bankCard, role=role)
                db.session.add(user)
                db.session.commit()

                return redirect(url_for('login'))
            else:
                err_email = err_email and "указанный email уже привязан"
                err_telephone = err_telephone and "указанный телефон уже привязан"
                nickname = None
        else:
            nickname = User.getNextNickname(nickname)

    return render_template("signin.html", form=form, title="SignIn", nickname=nickname,
                           err_email=err_email, err_telephone=err_telephone)

# protection from cross site request forgery
@app.before_request
def csrf_protect():
    g.user = current_user
    if request.method == 'POST':
        token = session.pop('_csrf_token', None)
        if not token or token != request.form.get('_csrf_token'):
            abort(403)

def some_random_string():
    """
    :return: сгенерированное значение строки длинной в 255 символов
    """
    alphavite = [chr(i) for i in range(ord('a'), ord('z'))]
    return "".join([choice(alphavite) for i in range(255)])

def generate_csrf_token():
    """
        герерирует секретный ключ, проверяя был ли он
        сгенерирован до этого.
    :return: csrf token
    """
    if '_csrf_token' not in session:
        session['_csrf_token'] = some_random_string()
    return session['_csrf_token']

# добавляем в глобальный контекст шаблонизатора jinja генератор
app.jinja_env.globals['csrf_token'] = generate_csrf_token
#end protection from csrf1
