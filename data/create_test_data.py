__author__ = 'alexey'
#!/usr/bin/sh

from app.models import *

from app import db

from json import dump, load
from app.utils.indexer import FileIndexer

class WrapperExcept(object):
    """
        Wrapper for catching error
        in work class
    """
    def __init__(self, msg):
        self.msg = msg

    def __call__(self, func):
        def wrapper(*args, **kargs):
            try:
                func(*args, **kargs)
            except:
                print(self.msg)
                return False
            return True
        return wrapper

class CreaterTestData(object):
    def __init__(self, filename):
        self.filename = filename

    def createElement(self, tablename, elem):
        """
            Create new element of table from json-object
        :param tablename: name of table in sqlalchemy
        :param elem: class sqlalchemy model for create new row
        :return: None
        """
        with open(self.filename) as file:
            for node in load(file)[tablename]:
                db.session.add(elem(**node))
                db.session.commit()

    @WrapperExcept('Error add first_name')
    def addFirstNames(self):
        self.createElement("first_name", FirstName)

    @WrapperExcept('Error add second_name')
    def addSecondNames(self):
        self.createElement("second_name", SecondName)

    @WrapperExcept('Error add password')
    def addPassword(self):
        self.createElement("password", Password)

    @WrapperExcept('Error add bank cards')
    def addBankCard(self):
        self.createElement('bank_cards', BankCard)

    @WrapperExcept('Error add user')
    def addUser(self):
        self.createElement('user', User)

    #@WrapperExcept('Error add product')
    def addProduct(self):
        self.createElement('product', Product)

    #@WrapperExcept('Error add computer')
    def addComputer(self):
        self.createElement('computer', Computer)

    #@WrapperExcept('Error add cpu')
    def addCpu(self):
        self.createElement('cpu', Cpu)

    #@WrapperExcept('Error add memory')
    def addMemory(self):
        self.createElement('memory', Memory)

    #@WrapperExcept('Error add hdd')
    def addHdd(self):
        self.createElement('hdd', Hdd)

    #@WrapperExcept('Error add video card')
    def addVideoCard(self):
        self.createElement('videoCard', VideoCard)

    #@WrapperExcept('Error add sound card')
    def addSoundCard(self):
        self.createElement('soundCard', SoundCard)

    #@WrapperExcept('Error add images')
    def addImages(self):
        listFiles = FileIndexer.getImagesFrom('./../app/static/img/')
        for file in listFiles:
            db.session.add(Image(url=file))
            db.session.commit()

    @WrapperExcept('Error add producer')
    def addProducer(self):
        self.createElement('producer', Producer)

    @WrapperExcept('Error add server')
    def addServer(self):
        self.createElement('server', Server)

class FactoryServer(object):
    def __init__(self, fact):
        self.factory = fact
    @WrapperExcept('Error add server')
    def createServer(self):
        self.factory.addProducer()
        self.factory.addServer()

class FactoryUser(object):
    def __init__(self, fact):
        self.factory = fact

    @WrapperExcept("Error add user")
    def createUser(self):
        self.factory.addFirstNames()
        self.factory.addSecondNames()
        self.factory.addPassword()
        self.factory.addBankCard()
        self.factory.addUser()

class FactoryComputer(object):
    def __init__(self, fact):
        self.factory = fact

    #@WrapperExcept("Error add computer")
    def createComputer(self):
        self.factory.addCpu()
        self.factory.addHdd()
        #self.factory.addMemory()
        #self.factory.addSoundCard()
        #self.factory.addVideoCard()
        #self.factory.addComputer()

class FactoryImage(object):
    def __init__(self, fact):
        self.factory = fact

    #@WrapperExcept('Error add image')
    def createImage(self):
        self.factory.addImages()


if __name__ == '__main__':
    test = CreaterTestData(r'/home/alexey/clone/internetstore/data/data.json')
    #factoryUser = FactoryUser(test)
    #factoryUser.createUser()

    #factoryImage = FactoryImage(test)
    #factoryImage.createImage()

    test.addProduct()
    #factoryComputer = FactoryComputer(test)
    #factoryComputer.createComputer()

    factoryServer = FactoryServer(test)
    factoryServer.createServer()