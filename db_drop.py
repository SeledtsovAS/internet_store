__author__ = 'alexey'

from app import db
from config import SQLALCHEMY_MIGRATE_REPO
import os

def delete_dir(dirname):
    for node in os.listdir(dirname):
        file = os.path.join(dirname, node)
        if not os.path.islink(file) and os.path.isdir(file):
            delete_dir(file)
        else:
            os.remove(file)
    os.rmdir(dirname)

if __name__ == '__main__':
    db.drop_all()
    if os.path.exists(SQLALCHEMY_MIGRATE_REPO):
        delete_dir(SQLALCHEMY_MIGRATE_REPO)

    print('Database is cleared!')